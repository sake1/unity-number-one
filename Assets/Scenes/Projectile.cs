﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	private Rigidbody rb;
	public int damage;
	public float force;
	public float lifetime;
	
	private void Awake() {
		rb = gameObject.GetComponent<Rigidbody>();
	}
	
	public void Fire() {
		rb.AddForce(transform.forward * force);
		StartCoroutine(RecycleObject(lifetime));
	}
	
	private IEnumerator RecycleObject(float expire) {
		yield return new WaitForSeconds(expire);
		rb.velocity = Vector3.zero;
		gameObject.SetActive(false);
	}
	
	void OnCollisionEnter(Collision other) {
		if(other.gameObject.tag == "Enemy") {
			EnemyController enemy = other.gameObject.GetComponent<EnemyController>();
			enemy.GetHit(damage);
			this.gameObject.SetActive(false);
		}
	}
}
