﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public int health;

	public void GetHit(int damage) {
		health -= damage;
		if(health == 0) {
			Destroy(this.gameObject);
		}
	}
}
