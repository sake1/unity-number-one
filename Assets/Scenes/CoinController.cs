﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {

	private int value = 10;

	public int GetHit() {
		Debug.Log("You get " + value + " point!");
		Destroy(this.gameObject);
		return value;
	}
}
