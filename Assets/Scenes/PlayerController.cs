﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	
	private int points = 0;
	
	public GameObject projectile;
	public int projectilePoolSize;
	private List<GameObject> projectilePool;

	void Start () {
		Debug.Log("Hello World");
		projectilePool = new List<GameObject>();
		for(int i = 0; i < projectilePoolSize; i++) {
			GameObject go = Instantiate(projectile);
			go.SetActive(false);
			
			projectilePool.Add(go);
		}
	}
	
	void Awake() {
		
	}
	
	GameObject getBullet() {
		foreach(GameObject go in projectilePool) {
			if(!go.activeSelf) {
				return go;
			}
		}
		return null;
	}
	
	void Update () {
		//float x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
		//float z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

		//transform.Rotate(z, x, 0);
		//transform.Translate(0, 0, z);
		
		if(Input.GetButton("Fire1")) {
			Vector3 playerPos = transform.position;
			Vector3 playerDirection = transform.forward;
			Quaternion playerRotation = transform.rotation;
			Vector3 spawnPos = playerPos + playerDirection ;
			//GameObject bullet = Instantiate(projectile, spawnPos, playerRotation);
			//bullet.GetComponent<Projectile>().Fire();
			//bullet.transform.position = this.transform.position;
			
			GameObject bullet = getBullet();
			if(bullet != null) {
				bullet.transform.position = spawnPos;
				bullet.transform.rotation = playerRotation;
				bullet.SetActive(true);
				bullet.GetComponent<Projectile>().Fire();
			}
		}
	}

	void OnCollisionEnter(Collision other) {
		if(other.gameObject.tag == "Enemy") {
			EnemyController enemy = other.gameObject.GetComponent<EnemyController>();
			enemy.GetHit(1);
		}
	}

	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Coin") {
			CoinController coin = other.gameObject.GetComponent<CoinController>();
			points += coin.GetHit();
			Debug.Log("You now have " + points + " points!");
		}
	}
}
